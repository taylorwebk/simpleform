import React from 'react'
import Axios from 'axios'

import { withStyles } from '@material-ui/core/styles'
import SwipeableViews from 'react-swipeable-views'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'

import Multiform from '../components/MultiForm'
import centerContent from '../utils/centerContent'

function TabContainer({ children }) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }} variant="h5">
      {children}
    </Typography>
  )
}


const styles = theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: '60%',
    marginTop: 18
  }
})

class FullWidthTabs extends React.Component {
  state = {
    value: 0,
    nro: 0
  }

  componentDidMount() {
    /* Axios.get('http://localhost:8081/get').then((res) => {
      this.setState({
        nro: res.data
      })
    }) */
  }

  handleChange = (event, value) => {
    this.setState({ value })
  }

  handleChangeIndex = (index) => {
    this.setState({ value: index })
  }

  render() {
    const { classes, theme } = this.props
    const { value, nro } = this.state

    return (
      <Paper className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
          >
            <Tab label="Formulario" />
            <Tab label="Resultados" />
          </Tabs>
        </AppBar>
        <SwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={value}
          onChangeIndex={this.handleChangeIndex}
        >
          <Multiform />
          <TabContainer dir={theme.direction}>
            {`Total de: ${nro} registros`}
          </TabContainer>
        </SwipeableViews>
      </Paper>
    )
  }
}

export default withStyles(styles, { withTheme: true })(centerContent(FullWidthTabs))
