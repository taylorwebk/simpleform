import React from 'react'

const styles = {
  display: 'flex',
  justifyContent: 'center'
}

const centerContent = Component => (
  props => (
    <div style={styles}>
      <Component {...props} />
    </div>
  )
)

export default centerContent
