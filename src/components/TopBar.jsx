import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

import strings from '../utils/strings'

const styles = {
  root: {
    flexGrow: 1
  },
  toolBar: {
    justifyContent: 'center'
  }
}

const TopBar = ({ classes }) => (
  <div className={classes.root}>
    <AppBar position="static">
      <Toolbar className={classes.toolBar}>
        <Typography variant="h5" color="inherit">
          { strings.mainTitle }
        </Typography>
      </Toolbar>
    </AppBar>
  </div>
)

export default withStyles(styles)(TopBar)
