import React, { useState } from 'react'
import FormControl from '@material-ui/core/FormControl'
import FormGroup from '@material-ui/core/FormGroup'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'

const InputCheckbox = ({ name = '', boxes = [] }) => {
  const [value, setValue] = useState([])
  const setVal = e => setValue([...value, e.target.value])
  const handleCheck = (e) => {
    if (value.indexOf(e.target.value) !== -1) {
      setValue(value.filter(item => item !== e.target.value))
    } else {
      setVal(e)
    }
  }
  return (
    <FormControl component="fieldset" fullWidth>
      <FormGroup>
        { boxes.map((box) => {
          const key = Object.keys(box)[0]
          return (
            <FormControlLabel
              key={key}
              name={name}
              control={
                <Checkbox checked={value.indexOf(key) !== -1} onChange={handleCheck} value={key} />
              }
              label={box[key]}
            />
          )
        })}
      </FormGroup>
    </FormControl>
  )
}

export default InputCheckbox
