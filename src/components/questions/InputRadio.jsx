import React, { useState } from 'react'
import FormControl from '@material-ui/core/FormControl'
import RadioGroup from '@material-ui/core/RadioGroup'
import Radio from '@material-ui/core/Radio'
import FormControlLabel from '@material-ui/core/FormControlLabel'

const InputText = ({ name = '', radios = [] }) => {
  const [value, setValue] = useState('')
  const setVal = e => setValue(e.target.value)
  return (
    <FormControl component="fieldset" fullWidth>
      <RadioGroup
        name={name}
        value={value}
        onChange={setVal}
      >
        {
          radios.map((radio) => {
            const key = Object.keys(radio)[0]
            return <FormControlLabel key={key} value={key} control={<Radio />} label={radio[key]} />
          })
        }
      </RadioGroup>
    </FormControl>
  )
}

export default InputText
