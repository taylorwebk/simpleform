import React, { useState } from 'react'
import TextField from '@material-ui/core/TextField'

import { withStyles } from '@material-ui/core/styles'

const styles = () => ({
  textField: {
    flexGrow: 1,
    flexBasis: 0
  }
})

const InputText = ({
  name = '', type = 'text', maxLength = 100, classes, getHelperText = () => null
}) => {
  const [value, setValue] = useState({
    text: '',
    helperText: null
  })
  const stringCapitalize = type == 'text' ? {
    textTransform: 'capitalize'
  } : {}

  const setVal = (e) => {
    const { target } = e
    let newText = target.value
    if (target.value.length > maxLength) {
      newText = target.value.slice(0, maxLength)
    }
    setValue({
      text: newText,
      helperText: getHelperText(newText)
    })
  }

  return (
    <TextField
      className={classes.textField}
      // error
      helperText={value.helperText}
      name={name}
      placeholder="Su respuesta..."
      value={value.text}
      onChange={setVal}
      type={type}
      margin="normal"
      inputProps={{
        style: stringCapitalize
      }}
    />
  )
}

export default withStyles(styles)(InputText)
