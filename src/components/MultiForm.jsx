import React from 'react'
import axios from 'axios'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core/styles'

import FormItem from './FormItem'
import InputText from './questions/InputText'

const styles = () => ({
  container: {
    padding: 24
  }
})

const getHelperTextString = limit => val => (
  <span component="span">
    {`Máximo ${limit} carácteres.`}
    <b>
      {` ${limit - (`${val}`).length} `}
      {'Restantes.'}
    </b>
  </span>
)

const getHelperTextOpts = opts => (val) => {
  return opts.map((opt) => {
    if (opt.id === val) {
      return <b>{` ${opt.id}:${opt.text}.  `}</b>
    }
    return ` ${opt.id}:${opt.text}.  `
  })
}

const questions = [
  {
    id: 0,
    text: 'C.I.:',
    component: (
      <InputText
        name="ci"
        type="number"
        maxLength={10}
        getHelperText={getHelperTextString(10)}
      />
    )
  },{
    id: 10,
    text: 'Direccion: ',
    component: <InputText name="di" maxLength={200} getHelperText={getHelperTextString(200)} />
  },
  {
    id: 1,
    text: 'Paterno: ',
    component: <InputText name="paterno" maxLength={20} getHelperText={getHelperTextString(20)} />
  }, {
    id: 2,
    text: 'Materno: ',
    component: <InputText name="materno" maxLength={20} getHelperText={getHelperTextString(20)} />
  }, {
    id: 3,
    text: 'Nombre(s): ',
    component: <InputText name="nombre" maxLength={30} getHelperText={getHelperTextString(30)} />
  }, {
    id: 4,
    text: 'Género: ',
    component: (
      <InputText
        name="genero"
        type="number"
        maxLength={1}
        getHelperText={getHelperTextOpts([
          { id: '1', text: 'Mujer' },
          { id: '2', text: 'Varon' }
        ])}
      />
    )
  }, {
    id: 5,
    text: 'Estado Civil: ',
    component: (
      <InputText
        name="estado"
        type="number"
        maxLength={1}
        getHelperText={getHelperTextOpts([
          { id: '1', text: 'Soltero/a' },
          { id: '2', text: 'Casado/a' },
          { id: '3', text: 'Divorciado/a' },
          { id: '4', text: 'Viudo/a' }
        ])}
      />
    )
  }, {
    id: 6,
    text: 'Fecha de Nacimiento: ',
    component: <InputText name="fnac" type="date" />
  }
]

const submitHandler = (e) => {
  e.preventDefault()
  const form = e.target
  const values = Object.values(form).reduce((obj, field) => {
    if (field.name === '' || field.name === undefined) {
      return obj
    }
    if (field.type === 'radio') {
      if (field.checked) {
        obj[field.name] = field.value // eslint-disable-line
      }
      return obj
    }
    if (field.type === 'checkbox') {
      if (obj[field.name]) {
        if (field.checked) {
          obj[field.name] = [...obj[field.name], field.value] // eslint-disable-line
        }
        return obj
      }
      if (field.checked) {
        obj[field.name] = [field.value] // eslint-disable-line
      } else {
        obj[field.name] = [] // eslint-disable-line
      }
      return obj
    }
    obj[field.name] = field.type === "number" ? parseInt(field.value) : field.value // eslint-disable-line
    return obj
  }, {})
  console.log(values)
  console.log(JSON.stringify(values))
  axios.post('http://localhost:8081/insert', JSON.stringify(values)).then((res) => {
    console.log(res)
    location.reload()
  })
}

const MultiForm = ({ classes }) => (
  <div className={classes.container}>
    <form onSubmit={submitHandler}>
      {
        questions.map(question => (
          <FormItem
            key={question.id}
            text={question.text}
            component={question.component}
          />
        ))
      }
      <br />
      <Button type="submit" variant="contained" color="primary" fullWidth> Enviar </Button>
    </form>
  </div>
)

export default withStyles(styles)(MultiForm)
