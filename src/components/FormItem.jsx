import React from 'react'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import { withStyles } from '@material-ui/core/styles'

const styles = () => ({
  container: {
    display: 'flex',
    flexGrow: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingBottom: 5
  },
  question: {
    flexGrow: 1,
    flexBasis: 0
  }
})

const FormItem = ({ classes, text, component }) => (
  <div className={classes.container}>
    <Typography variant="subtitle1" color="textPrimary" inline className={classes.question}>
      {text}
    </Typography>
    {component}
  </div>
)

export default withStyles(styles)(FormItem)
// helperText="Some important text", error
