import React from 'react'
import { hot } from 'react-hot-loader/root'

import TopBar from './components/TopBar'
import Main from './screens/Main'
import themeProvider from './utils/themeProvider'

import './fonts.css'

const App = () => (
  <React.Fragment>
    <TopBar />
    <Main />
  </React.Fragment>
)

export default hot(themeProvider(App))
